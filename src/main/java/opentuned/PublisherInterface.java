package opentuned;

public interface PublisherInterface {
    String name();
    void publish(Notification notification);
}

class Publisher implements PublisherInterface {

    private String name;
    private HubInterface hub;

    public Publisher(String name, HubInterface hub) {
        this.name = name;
        this.hub = hub;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void publish(Notification notification) {

    }
}
