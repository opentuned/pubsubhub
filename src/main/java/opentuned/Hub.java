package opentuned;

import com.google.common.annotations.VisibleForTesting;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Hub implements HubInterface {

    private static HashMap<String, Set<SubscriberInterface>> subMappings = new HashMap<>();

    @VisibleForTesting
    public Set<SubscriberInterface> getSubscribers(String sub) {
        return subMappings.get(sub);
    }

    @Override
    public void register(SubscriberInterface subscriber) {
        Set<SubscriberInterface> subscribersSet = new HashSet<>();
        subscribersSet.add(subscriber);
        subMappings.put(subscriber.name(), subscribersSet);
    }

    @Override
    public void deRegister(SubscriberInterface subscriberInterface) {

    }

    @Override
    public void register(PublisherInterface publisherInterface) {

    }

    @Override
    public void deRegister(PublisherInterface publisherInterface) {

    }
}
