package opentuned;

public class Notification {
    private final String foo;

    public Notification(String foo) {
        this.foo = foo;
    }

    public String getFoo() {
        return foo;
    }
}
