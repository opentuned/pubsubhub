package opentuned;

public interface HubInterface {
    void register(SubscriberInterface subscriberInterface);
    void deRegister(SubscriberInterface subscriberInterface);
    void register(PublisherInterface publisherInterface);
    void deRegister(PublisherInterface publisherInterface);
}
