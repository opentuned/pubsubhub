package opentuned;

public interface SubscriberInterface {
    String name();
    void receive(Notification notification);
}

class Subscriber implements SubscriberInterface {

    private final String name;

    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void receive(Notification notification) {

    }
}