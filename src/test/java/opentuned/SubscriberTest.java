package opentuned;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubscriberTest {

    @Test
    void name() {
        SubscriberInterface underTest = new Subscriber("mySub1");
        assertEquals("mySub1", underTest.name());
    }

    @Disabled
    @Test
    void receive() {
    }
}