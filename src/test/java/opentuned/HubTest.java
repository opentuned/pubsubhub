package opentuned;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HubTest {

    @Test
    void registerSubscriber() {
        Hub underTest = new Hub();
        SubscriberInterface sub = new Subscriber("mySub1");
        underTest.register(sub);
        assertEquals(1, underTest.getSubscribers("mySub1").size());
    }

    @Disabled
    @Test
    void deRegister() {
    }

    @Disabled
    @Test
    void register1() {
    }

    @Disabled
    @Test
    void deRegister1() {
    }
}