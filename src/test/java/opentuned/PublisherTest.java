package opentuned;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class PublisherTest {

    @Test
    void name() {
        HubInterface mockHub = mock(Hub.class);
        Publisher underTest = new Publisher("pub1", mockHub);
        assertEquals(underTest.name(), "pub1");
    }

    @Disabled
    @Test
    void publish() {
    }
}